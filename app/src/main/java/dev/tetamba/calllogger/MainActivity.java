package dev.tetamba.calllogger;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import dev.tetamba.calllogger.helper.SharedPreferencesUtil;
import dev.tetamba.calllogger.receiver.CallReceiver;
import dev.tetamba.calllogger.receiver.LocationReceiver;
import dev.tetamba.calllogger.service.LocationMonitoringService;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!checkIfAlreadyhavePermission()) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    },
                    1);
        } else {
            setupAndFinish();
        }

    }

    private void setupAndFinish() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
            @SuppressLint("MissingPermission") String id = telephonyManager.getDeviceId();

            // REAL IMEI
            //SharedPreferencesUtil.setDeviceId(id);
            
            SharedPreferencesUtil.setDeviceId("000000000000000");

        } catch (Exception e) {
            Log.e("DEVICEID", e.getMessage());
            SharedPreferencesUtil.setDeviceId("000000000000000");
        }

        Intent intent = new Intent(this, LocationMonitoringService.class);
        startService(intent);

        // IMPLICIT RECEIVER DECLARATION (for Android O)
        LocalBroadcastManager.getInstance(this).registerReceiver(new LocationReceiver(), new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST));
//        LocalBroadcastManager.getInstance(this).registerReceiver(new CallReceiver(), new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL));

        Toast.makeText(getApplicationContext(), "Service Started", Toast.LENGTH_SHORT).show();

        PackageManager p = getPackageManager();
        p.setComponentEnabledSetting(getComponentName(), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

        finish();
    }

    private boolean checkIfAlreadyhavePermission() {
        int result1 = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        int result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);

        return result1 == PackageManager.PERMISSION_GRANTED && result2 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    setupAndFinish();

                } else {
                    Toast.makeText(getApplicationContext(), "Cannot Start Service Without Permissions", Toast.LENGTH_SHORT).show();
                    finish();

                }
            }
        }
    }
}
