package dev.tetamba.calllogger.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.firestore.GeoPoint;

import dev.tetamba.calllogger.BaseApp;

public class SharedPreferencesUtil {
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    static {
        sharedPreferences = BaseApp.getInstance()
                .getSharedPreferences(
                        BaseApp.class.getSimpleName()
                        , Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public static void setCurrentLocation(String s) {
        editor.putString(Constants.USER_LOCATION, s).apply();
    }

    public static void setCurrentGeo(String latitude, String longitude) {
        editor
                .putString(Constants.LOC_LATITUDE, latitude)
                .putString(Constants.LOC_LONGITUDE, longitude)
                .apply();
    }

    @Nullable
    public static GeoPoint getCurrentGeo() {
        try {
            return new GeoPoint(
                Double.parseDouble(sharedPreferences.getString(Constants.LOC_LATITUDE, "")),
                Double.parseDouble(sharedPreferences.getString(Constants.LOC_LONGITUDE, ""))
            );
        } catch (Exception e) {
            Log.e("CURRENTGEO", e.getMessage());
            return null;
        }
    }

    public static String getCurrentLocation() {
        return sharedPreferences.getString(Constants.USER_LOCATION, "undefined");
    }

    public static void addLog(String log) {
        String _log = String.format("------------------------------\n%s\n", log);

        editor.putString(Constants.LOGS, _log + getLogs()).apply();
    }

    public static String getLogs() {
        return sharedPreferences.getString(Constants.LOGS, "");
    }

    public static void setDeviceId(String id) {
        editor.putString(Constants.DEVICEID, id).apply();
    }

    public static String getDeviceId() {
        return sharedPreferences.getString(Constants.DEVICEID, "");
    }

    public static void clearLogs() {
        editor.putString(Constants.LOGS, "").apply();
    }
}
