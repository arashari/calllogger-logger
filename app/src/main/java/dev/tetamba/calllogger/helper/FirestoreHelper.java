package dev.tetamba.calllogger.helper;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.GeoPoint;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dev.tetamba.calllogger.BaseApp;

public class FirestoreHelper {
    private static BaseApp baseApp;

    static {
        baseApp = BaseApp.getInstance();
    }

    public static void addLog(String imei, String type, String number, GeoPoint loc, Date date) {
        Map<String, Object> log = new HashMap<>();
        log.put("imei", imei);
        log.put("type", type);
        log.put("number", number);
        log.put("loc", loc);
        log.put("date", new Timestamp(date));

        baseApp.getDb()
                .collection("logs")
                .document()
                .set(log)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
//                        Log.d("FIRESTORE", "DocumentSnapshot successfully written!");
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("FIRESTORE", "Error writing document", e);
                    }
                });
    }
}
