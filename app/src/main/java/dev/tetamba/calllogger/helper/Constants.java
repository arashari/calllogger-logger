package dev.tetamba.calllogger.helper;

public class Constants {
    static final String USER_LOCATION = "userLocation";
    static final String LOGS = "logs";
    static final String DEVICEID = "deviceId";
    static final String LOC_LATITUDE= "locLatitude";
    static final String LOC_LONGITUDE= "locLongitude";

}
