package dev.tetamba.calllogger.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import dev.tetamba.calllogger.helper.SharedPreferencesUtil;
import dev.tetamba.calllogger.service.LocationMonitoringService;

public class LocationReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
        String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);

        Log.d("LOCATION", String.format("%s, %s", latitude, longitude));
        if (latitude != null && longitude != null) {
            try {
                Geocoder geocoder = new Geocoder(context, Locale.getDefault());

                List<Address> addresses = geocoder.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
                String locStr = String.format("lat: %s, long: %s - (%s)", latitude, longitude, addresses.get(0).getSubAdminArea());

                SharedPreferencesUtil.setCurrentLocation(locStr);
                SharedPreferencesUtil.setCurrentGeo(latitude, longitude);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
