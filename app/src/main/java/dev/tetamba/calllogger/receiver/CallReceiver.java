package dev.tetamba.calllogger.receiver;

import android.content.Context;

import com.gabesechan.android.reusable.receivers.PhonecallReceiver;

import java.util.Date;

import dev.tetamba.calllogger.helper.FirestoreHelper;
import dev.tetamba.calllogger.helper.SharedPreferencesUtil;

class TAG {
    public static String ONINCOMINGCALLSTART = "INCOMINGCALLSTART";
    public static String ONINCOMINGCALLEND = "INCOMINGCALLEND";

    public static String ONOUTGOINGCALLSTART = "OUTGOINGCALLSTART";
    public static String ONOUTGOINGCALLEND = "OUTGOINGCALLEND";

    public static String ONMISSEDCALL = "MISSEDCALL";
}

public class CallReceiver extends PhonecallReceiver {

    private void logBuilder(String tag, String number, Date date) {
        String location = SharedPreferencesUtil.getCurrentLocation();
        String log = String.format("[%s]\n" +
                "imei: %s\n" +
                "number: %s\n" +
                "date: %s\n" +
                "%s", tag, SharedPreferencesUtil.getDeviceId(), number, date, location);

//        Log.d("CALL LOGGER", log);
//        SharedPreferencesUtil.addLog(log);

        FirestoreHelper.addLog(
                SharedPreferencesUtil.getDeviceId(),
                tag,
                number,
                SharedPreferencesUtil.getCurrentGeo(),
                date);
    }

    @Override
    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
//        Log.d(TAG.ONINCOMINGCALLSTART, String.format("%s, %s", start.toString(), number));

        logBuilder(TAG.ONINCOMINGCALLSTART, number, start);
    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, final String number, final Date start) {
//        Log.d(TAG.ONOUTGOINGCALLSTART, String.format("%s, %s", start.toString(), number));

        logBuilder(TAG.ONOUTGOINGCALLSTART, number, start);
    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
//        Log.d(TAG.ONINCOMINGCALLEND, String.format("%s, %s", start.toString(), number));

        logBuilder(TAG.ONINCOMINGCALLEND, number, start);
    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
//        Log.d(TAG.ONOUTGOINGCALLEND, String.format("%s, %s", start.toString(), number));

        logBuilder(TAG.ONOUTGOINGCALLEND, number, start);
    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start) {
//        Log.d(TAG.ONMISSEDCALL, String.format("%s, %s", start.toString(), number));

        logBuilder(TAG.ONMISSEDCALL, number, start);
    }
}
