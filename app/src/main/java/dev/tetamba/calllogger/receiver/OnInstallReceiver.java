package dev.tetamba.calllogger.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import dev.tetamba.calllogger.service.LocationMonitoringService;

public class OnInstallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, LocationMonitoringService.class);
        context.startService(i);

        // IMPLICIT RECEIVER DECLARATION (for Android O)
        LocalBroadcastManager.getInstance(context).registerReceiver(new LocationReceiver(), new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST));
//        LocalBroadcastManager.getInstance(context).registerReceiver(new CallReceiver(), new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL));


        Log.i("CALLLOGGER", "service started");
    }
}
